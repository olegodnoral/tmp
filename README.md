# Application Process (TypeScript Task)

## Task Goal / Rating

Please consider the following hints on the goals of this task and which points we focus on during review:

* This task description might contain more tasks then you can achieve in 2 hours to accomodate for the different amount of experience. Not finishing all of the tasks is **not** meaning that you did not pass the task.
* Focus on pushing running code per task. Rather push less code with higher quality then a large amount of code that is not running correctly
* It's fine to make compromises on a coding challenge with limited time. We'd be happy to talk about them in the interview after the coding challenge.

## Task Description

**Scenario**

In this task you are going to implement a small fullstack example by showing data in Material design table which is dynamically generated in the backend. The tasks are
structured in individual steps so that you can have a running version even if you do not complete all of them.

The repository is a NWRL monorepo and has an Angular frontend and a Nest.js backend application. The intial setup was already performed for you!


### Task

1. Create a paginated table in Material Design in the frontend
    * The table should the Material Design library
    * The tabe should have the following rows
      * ID = numeric (incremental)
      * Title = string (max. 255 characters)
      * Criticality = numeric (0.0 - 10.0)
    * You can generate random data in the frontend, use a static array or just continue with the next task immediately
    * Please generate enough data so that multiple pages are visible

2. Create an API endpoint to load the data from the backend
    * You can define the route on your own as you like
    * Please consider the data definition in taks 1 and generate random data for the model
    * Load the data from the endpoint and display it asynchronuously in the table
    * You can generate the random data dynamically during the start of the backend and there is no need to use a database!

3. Allow to filter and sort the random data
    * Add the search and filter header to the table in the frontend
    * Implement corresponding parameters for your API endpoint to allow sorting and filtering the data

Finally if you still have time left add tests to your code.
