import { Injectable } from '@nestjs/common';
import { ItemRowDto } from "./models/item-row.dto";
import { QueryParamsDto } from "./models/query-params.dto";

@Injectable()
export class ItemsService {

  private readonly fieldsForFiltration = [ 'ID', 'Title', 'Criticality' ];
  private readonly stringFields = ['Title'];

  private data: ItemRowDto[] = [ ...Array(300).keys() ]
    .map((_, index) => ItemRowDto.generateMockItem(index + 1))

  public loadData(query: QueryParamsDto): { data: ItemRowDto[], total: number } {
    let tmpData = this.sortItem(query.sort_field, query.sort_order, this.data);
    tmpData = this.filterData(query, tmpData);
    return {
      data: tmpData.slice(query.pageSize * query.pageIndex, query.pageSize * (query.pageIndex + 1)),
      total: tmpData.length
    };
  }

  private sortItem(sort_field: string, sort_order: string, data: ItemRowDto[]): ItemRowDto[] {
    return data.sort((a, b) =>
      a?.[sort_field] > b?.[sort_field] ? (sort_order === 'asc' ? 1 : -1) :
        a?.[sort_field] < b?.[sort_field] ? (sort_order === 'asc' ? -1 : 1) : 0)
  }

  private filterData(query: QueryParamsDto, data: ItemRowDto[]): ItemRowDto[] {
    const filtration = this.prepareDataForFiltration(query);

    let result = [...data];
    filtration.forEach((item: {[key:string]: string|number}) => {
      const filed = Object.keys(item)[0]
      result = result.filter((row) => {
        if (this.stringFields.includes(filed)) {
          return row[filed].toLowerCase().includes(item[filed].toString().toLowerCase());
        }
        return row[filed] === item[filed];
      })
    })
    return result;
  }

  private prepareDataForFiltration(query: QueryParamsDto): {[key:string]: string|number}[] {
    const filtration = [];

    for (const [ key, value ] of Object.entries(query)) {
      if (this.fieldsForFiltration.includes(key)) {
        const tmp = {}
        tmp[key] = value
        filtration.push(tmp)
      }
    }
    return filtration;
  }

}
