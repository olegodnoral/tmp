import { randomInt, randomBytes } from 'crypto';

export class ItemRowDto {
  ID: number;
  Title: string;
  Criticality: number;


  static getRandomFloat(min, max): number {
    return Math.random() * (max - min) + min;
  }

  static generateMockItem(ID: number = randomInt(999)): ItemRowDto {
    return {
      ID,
      Title: randomBytes(255).toString('hex').substr(0, 255),
      Criticality: +this.getRandomFloat(0.0, 10.0).toFixed(1)
    }
  }
}
