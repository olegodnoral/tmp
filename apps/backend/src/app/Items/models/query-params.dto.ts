import { Type } from "class-transformer";

export class QueryParamsDto {

  @Type(() => Number)
  ID?: number;

  @Type(() => String)
  Title?: string;

  @Type(() => Number)
  Criticality?: number;

  @Type(() => String)
  sort_field?: string;

  @Type(() => String)
  sort_order?: string;

  @Type(() => Number)
  pageIndex?: number;

  @Type(() => Number)
  pageSize?: number;
}
