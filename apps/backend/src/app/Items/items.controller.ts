import { Controller, Get, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { ItemsService } from "./items.service";
import { ItemRowDto } from "./models/item-row.dto";
import { QueryParamsDto } from "./models/query-params.dto";

@Controller('items')
export class ItemsController {

  constructor(private readonly itemsService: ItemsService) {}

  @Get()
  @UsePipes(new ValidationPipe({ transform: true }))
  loadData(@Query() QueryParamsDto: QueryParamsDto): { res: { data: ItemRowDto[], total: number } } {
    return { res: this.itemsService.loadData(QueryParamsDto) };
  }
}
