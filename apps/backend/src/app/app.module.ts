import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ItemsModule } from "./Items/items.module";

@Module({
  imports: [
    ItemsModule,
    RouterModule.register([
      {
        module: ItemsModule,
        path: 'items-data'
      }
    ])
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
