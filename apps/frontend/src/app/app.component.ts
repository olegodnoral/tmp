import { AfterViewInit, ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { FormControl } from "@angular/forms";
import { combineLatest, debounceTime, mergeMap, startWith, takeUntil } from "rxjs";
import { DestroyService } from "./destroy.service";
import { DataService } from "./data.service";
import { IFilter, IPagination, IRowData, ISort } from "./models/models";


@Component({
  selector: 'cs-fullstack-typescript-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ DestroyService ]
})
export class AppComponent implements AfterViewInit {
  @ViewChild(MatPaginator) private paginator: MatPaginator | undefined;
  @ViewChild(MatSort) private sort: MatSort | undefined;

  public dataSource: MatTableDataSource<IRowData> = new MatTableDataSource<IRowData>();
  public filterID = new FormControl();
  public filterTitle = new FormControl();
  public filterCriticality = new FormControl();
  public resultsLength = 0;
  public readonly displayedColumns: string[] = [ 'ID', 'Title', 'Criticality' ];
  public readonly displayedColumnFilters: string[] = [ 'filter-ID', 'filter-Title', 'filter-Criticality' ];

  constructor(
    private readonly destroy$: DestroyService,
    private readonly data: DataService,
  ) {}

  ngAfterViewInit() {

    combineLatest([
      this.filterID.valueChanges.pipe(startWith(null)),
      this.filterTitle.valueChanges.pipe(startWith(null)),
      this.filterCriticality.valueChanges.pipe(startWith(null)),
      this.sort?.sortChange.pipe(startWith(null)),
      this.paginator?.page.pipe(startWith(null)),
    ])
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(15),
        mergeMap(() => {
          const filterData: IFilter = {
            ID: this.filterID.value,
            Title: this.filterTitle.value,
            Criticality: this.filterCriticality.value
          };
          const sortData: ISort = { active: this.sort?.active, direction: this.sort?.direction }
          const paginationData: IPagination = {
            pageIndex: this.paginator?.pageIndex,
            pageSize: this.paginator?.pageSize
          }
          return this.data.fetchData(filterData, paginationData, sortData);
        }),
      )
      .subscribe((data) => {
        this.dataSource.data = data.data;
        this.resultsLength = data.total;
      })
  }

}
