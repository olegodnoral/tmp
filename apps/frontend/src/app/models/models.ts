import { SortDirection } from "@angular/material/sort/sort-direction";

export interface IRowData {
  Title: string;
  ID: number;
  Criticality: number;
}

export interface IFilter {
  ID?: number,
  Title?: string,
  Criticality?: number
}

export interface ISort {
  active?: string,
  direction?: SortDirection
}

export interface IPagination {
  pageIndex?: number,
  pageSize?: number
}

export interface IDataResponse {
  data: IRowData[],
  total: number
}
