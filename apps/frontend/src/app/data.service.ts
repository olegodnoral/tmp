import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { IDataResponse, IFilter, IPagination, IRowData, ISort } from "./models/models";
import { map, Observable } from "rxjs";


@Injectable({
  providedIn: 'root',
})
export class DataService {

  private readonly backUrl = ' http://localhost:3333/api/items-data/items'

  constructor(private readonly http: HttpClient) {}

  public fetchData(filter: IFilter, pagination: IPagination, sort: ISort): Observable<IDataResponse> {
    const query = this.filterTransformer(filter, pagination, sort);
    return this.http.get<{ res: {data: IRowData[], total: number} }>(`${ this.backUrl }?${ query }`).pipe(map(res => res.res));
  }

  private filterTransformer(filter: IFilter, pagination: IPagination, sort: ISort): string {
    let query = '';
    for (const [key, value] of Object.entries(filter)) {
      if (value) query += `${ key }=${ encodeURI(value) }&`;
    }

    if (sort.active && sort.direction !== '') {
      query += `sort_field=${ sort.active }&sort_order=${ sort.direction }&`;
    }

    query += `pageIndex=${ pagination.pageIndex }&pageSize=${ pagination.pageSize }`;

    return query
  }

}
